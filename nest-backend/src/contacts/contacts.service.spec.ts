import { Test, TestingModule } from '@nestjs/testing';
import { ContactsService } from './contacts.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Contact } from './contacts.entity';
import { Repository } from 'typeorm';
import { MockType } from 'src/type/test.type';

const mockRepository = jest.fn(() => ({
  find: jest.fn(entity => entity),
}));

describe('ContactsService', () => {
  let service: ContactsService;
  let contactRepo: MockType<Repository<Contact>>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ContactsService,
        {
          provide: getRepositoryToken(Contact),
          useFactory: mockRepository,
        },
      ],
    }).compile();

    service = module.get<ContactsService>(ContactsService);
    contactRepo = module.get(getRepositoryToken(Contact));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should find all contacts', async () => {
    const contacts = [
      {
        id: 1,
        firstName: 'test',
        lastName: 'test',
        email: 'test@mail.com',
        phone: '0123456789',
        city: 'BKK',
        country: 'Thailand',
      },
    ] as Contact[];

    contactRepo.find.mockReturnValue(contacts);
    const result = await service.findAll();
    expect(result).toEqual(contacts);
    expect(result.length).toEqual(contacts.length);
  });
});
