import { Test, TestingModule } from '@nestjs/testing';
import { ContactsController } from './contacts.controller';
import { ContactsService } from './contacts.service';
import { Contact } from './contacts.entity';

const contacts = [
  {
    id: 1,
    firstName: 'test',
    lastName: 'test',
    email: 'test@mail.com',
    phone: '0123456789',
    city: 'BKK',
    country: 'Thailand',
  },
] as Contact[];

const mockContactService = jest.fn(() => ({
  findAll: jest.fn(() => contacts),
}));

describe('Contacts Controller', () => {
  let controller: ContactsController;
  let service: ContactsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ContactsController],
      providers: [
        {
          provide: ContactsService,
          useFactory: mockContactService,
        },
      ],
    }).compile();

    controller = module.get<ContactsController>(ContactsController);
    service = module.get(ContactsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('access /contract [GET]', async () => {
    const res = await controller.index();
    expect(res).toEqual(contacts);
  });
});
