import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { Contact } from './contacts.entity';
import { ContactsService } from './contacts.service';
import { ApiUseTags, ApiImplicitParam, ApiImplicitBody } from '@nestjs/swagger';

@ApiUseTags('contacts')
@Controller('contacts')
export class ContactsController {

    constructor(private contactsService: ContactsService) {}

    @Get()
    index(): Promise<Contact[]> {
        return this.contactsService.findAll();
    }

    @ApiImplicitParam({ name: 'id'})
    @Get(':id')
    getById(@Param('id') id): Promise<Contact> {
        return this.contactsService.getById(id);
    }

    @Post()
    async create(@Body() contactData: Contact): Promise<any> {
        return this.contactsService.create(contactData);
    }

    @ApiImplicitParam({ name: 'id' })
    @Put(':id')
    async update(@Param('id') id, @Body() contactData: Contact) {
        contactData.id = Number(id);
        return this.contactsService.update(contactData);
    }

    @ApiImplicitParam({ name: 'id' })
    @Delete(':id')
    async delete(@Param('id') id) {
        return this.contactsService.delete(id);
    }

    @ApiImplicitBody({ name: 'contactData', isArray: true, type: Contact })
    @Post('multi')
    async createMulti(@Body() contactData: Contact[]) {
        return this.contactsService.createMultiple(contactData);
    }
}
