export type MockType<T> = {
    [p in keyof T]: jest.Mock<{}>;
};
