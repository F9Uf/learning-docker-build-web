import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ContactsModule } from './contacts/contacts.module';
import { TypeOrmModule} from '@nestjs/typeorm';
import { DbConfig } from './config/db.config';

@Module({
  imports: [ContactsModule,
  TypeOrmModule.forRoot({
    type: 'mysql',
    host: DbConfig.DB_HOST,
    username: DbConfig.DB_USERNAME,
    password: DbConfig.DB_PASSWORD,
    port: DbConfig.DB_PORT,
    database: DbConfig.DB_DATABASE,
    synchronize: true,
    entities: [__dirname + '/**/*.entity{.ts,.js}'],
  }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
